%\documentclass[12pt,a4paper]{article}
\documentclass[american]{article}
\usepackage{float}
\usepackage{babel}
\usepackage[utf8]{inputenc}
\usepackage{uebungen}
\usepackage{amsmath}
\usepackage{tikz}
\usepackage{graphicx}
\usepackage{listings}
\usepackage{amssymb}
\usepackage{hyperref}

\lstset{language=C++, basicstyle=\ttfamily,
  keywordstyle=\color{black}\bfseries, tabsize=4, stringstyle=\ttfamily,
  commentstyle=\it, extendedchars=true, escapeinside={/*@}{@*/}}

\title{\textbf{The DUNE Grid Interface -- Exercise \\Tuesday, Feb~24\textsuperscript{th}~2015 \\14:00-17:30}}
\dozent{Ole Klein, Adrian Ngo}
\institute{IWR, University of Heidelberg}
\semester{}
\abgabetitle{}
%\abgabeinfo{ in der Übung}
\uebungslabel{Exercise}
\blattlabel{DUNE Workshop 2015}


\newcommand{\vx}{\vec x}
\newcommand{\uu}{\textbf u}
\newcommand{\nn}{\textbf n}
\newcommand{\grad}{\vec \nabla}
\newcommand{\wind}{\vec \beta}
\newcommand{\Laplace}{\Delta}
\newcommand{\mycomment}[1]{}

\begin{document}

\blatt{}{}

{\bf General Remarks}

\lstset{language=bash}

The following remarks are valid for all the exercises during the course.

DUNE uses \lstinline!CMake! as a build system. In 
\lstinline!CMake!, there is a clear distinction between source code files and build results.
They are located in separate directories. When working on an exercise you should open a terminal
and switch to the build directory of the exercise:

\begin{lstlisting}
  [user@dunevm ~]$ cd ciit-workshop
  [user@dunevm ciit-workshop]$ cd release-build
  [user@dunevm release-build]$ cd grid-exercise
\end{lstlisting}

For later exercises, pick the correct folder for the exercise instead of \lstinline!grid-exercise!.
Your are now in your projects build directory. This is where you call the compiler using \lstinline!make!.
To switch to the corresponding source directory, you can follow the symlink \lstinline!src_dir!:

\begin{lstlisting}
  [user@dunevm grid-exercise]$ cd src_dir
\end{lstlisting}

To switch back to the build directory from the source directory, type \lstinline!cd ..!.
If, at any time, you want to debug your program using a debugger like \lstinline!gdb!,
you should switch to the build directory \lstinline!debug-build! instead of \lstinline!release-build!.

In the source directory, you should see (using \lstinline!ls!) the source file \lstinline!exercise.cc!,
which is the relevant code for this exercise. You build it like this:
\begin{lstlisting}
  [user@dunevm grid-exercise]$ make
\end{lstlisting}

Running \lstinline!make! will build your application (in this case: the exercise). The resulting output
is an executable, which in this cases is called \lstinline!exercise1!. You can execute it like this:

\begin{lstlisting}
  [user@dunevm grid-exercise]$ ./exercise1
\end{lstlisting}


\begin{uebung}{\bf First Steps with the DUNE Grid Interface}

Open the file \texttt{exercise1.cc} in a text editor.  It is an
example code that creates a structured grid (using the DUNE class
\texttt{YaspGrid}), and a mapper on this grid, assigning an index
to each element.  Then, it iterates over all elements of this grid and
over all intersections of each element.  The code is meant to print
some information about the grid cells and the intersections (but it
does not yet).  The file is intermingled by suggestions what to print.
You are invited to follow these suggestions or to try any of the
member functions you learned about in the lectures.

\footnotesize{
The DUNE class documentation can be found in 
\url{http://www.dune-project.org/doc-2.3.1/doxygen/html/}.\\
For this exercise, the geometry class plays a special role:\\
\url{http://www.dune-project.org/doc-2.3.1/doxygen/html/classDune_1_1Geometry-members.html}
}

\end{uebung}

\begin{uebung}{\bf The Finite Volume Method for the Transport Equation}

The partial differential equation considered in this exercise is the
\emph{linear transport equation},
\begin{equation}
  \label{eq:trans}
  \begin{split}
    \partial_t c(x, t) + \nabla\cdot\big(\uu(x)\,c(x,t)\big) &= 0 \\
    c(x,t) &= c_\text{in}(x,t)
  \end{split}
  \qquad
  \begin{split}
    &\text{in }\Omega, \\
    &\text{on }\Gamma_\text{in}.
  \end{split}
\end{equation}
The unknown solution is denoted by $c(x,t)$ and the velocity field by
$\uu(x)$.  The domain $\Omega$ is some open subset of $\mathbb{R}^d$.
For this exercise, we choose $d=2$ where intersections are $1$-D edges.
For $d=3$, intersections would be $2$-D faces.
The inflow boundary $\Gamma_\text{in}$ is the set of points $x$ on the
boundary of $\Omega$ for which the velocity vector $\uu(x)$ points
inwards.

We would like to numerically solve this equation by a cell-centered
finite volume scheme.  We discretize the domain $\Omega$ by a
triangulation $T_h$ and approximate the solution $c$ by a function
$c_h$ that is constant on each cell $E\in T_h$.  We denote the value
of $c_h$ on a cell $E$ by $c_E$.

Using the explicit Euler time discretization, the scheme can be
written as
\begin{equation}
  c_E(t_{k+1}) = c_E(t_k) - \frac{t_{k+1}-t_k}{|E|}
  \sum_{e\subset\partial E}|e|\;c^e(t_k)\;\nn_E^e\cdot\uu^e.
\end{equation}
The notation is as follows: $|E|$ is the area of the cell $E$, the
sum runs over all intersections $e$ of $E$ with either the boundary or
a neighboring cell, $|e|$ is the length of edge $e$, $\nn_E^e$ is the
outer normal of edge $e$ and $\uu^e$ is the velocity at the center of
edge $e$.  Finally, $c^e$ denotes the upwind concentration.  If
$\nn_E^e\cdot\uu^e>0$, this is $c_E$.  Otherwise it is either the
concentration in the neighboring cell or given by the boundary
condition $c_\text{in}$, depending on the location of $e$.

 \emph{Complete the implementation of the method in the files
  \texttt{exercise2.cc} and \texttt{fv.hh}.}

  An implementation of the scheme at hand has to store the values of
  the concentration on each cell for the current time step.  In the
  example code, a \texttt{std::vector<double>} is used for this
  purpose, see the typedef for \texttt{ScalarField} in
  \texttt{exercise2.cc}.  We use a mapper to get a consecutive
  numbering of the cells of the grid.  If the variable \texttt{e}
  holds some entity of codimension 0, the concentration value in this
  entity is \texttt{c[mapper.map(e)]}.

  At each time step, an update to the vector of concentrations has to
  be computed.  This is done by the \texttt{update()} member function
  of the class \texttt{FiniteVolume} in the file \texttt{fv.hh}.  This
  function iterates over all cells of the grid in order to compute an
  update for each cell.  Your task is to implement the computation of
  the update \texttt{up[cell\_index]}.  To this end, the code has to
  iterate over all intersections of the current cell.  For each
  intersection, the flux $|e|\nn_E^e\cdot\uu^e/|E|$ is to be
  calculated.  Depending on the sign of this flux, the upwind decision
  can be made.

%  In this part of the exercise, you do not need to change the file
%  \texttt{exercise2.cc} (of course you may!).
  \emph{Find the section where the \texttt{VTKWriter} is used to visualize the computation results.
    Check which header was required.}
  For each time step, the results are written to the files
  \lstinline!c-0xyz.vtu! in the build-directory you executed the program
  in.
  Call \\
  \\
  \texttt{paraview --data=c-..vtu}\\
  \\
  to load the output of all available time steps and push the ``Apply''- and
  ``Play''-buttons to visualize the result.
\end{uebung}

\end{document}

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:
