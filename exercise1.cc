// DUNE Workshop Feb 2015

// exercise1.cc
// Learn the fowllowing DUNE concepts:
// grid, grid view, entity, iterator, mapper, exception handling

#include "config.h"
#include <iostream>
#include <dune/common/exceptions.hh>
#include <dune/common/fvector.hh>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/grid/yaspgrid.hh>
#include <dune/grid/common/mcmgmapper.hh>

template<int dim>
struct DGLayout
{
    bool contains (Dune::GeometryType gt)
    {
        return gt.dim() == dim;
    }
};

int main(int argc, char** argv)
{
    try
    {
        Dune::MPIHelper::instance(argc, argv);

        // typedef for grid of preferred type
        static const int dim = 2;
        typedef Dune::YaspGrid<dim> GridType;

        // instantiate YaspGrid on the unit square with 4 by 4 cells
        Dune::FieldVector<double,dim> L(1.0);
        Dune::array<int,dim> N(Dune::fill_array<int,dim>(4));
        std::bitset<dim> periodic(false);

        GridType grid(L, N, periodic, 0);

        // Extract the grid view we would like to use
        // You could use a different grid view here -- the grid itself is not used anymore
        auto gridview = grid.leafGridView();

        // mapper for elements (codim=0)
        typedef GridType::LeafGridView GridView;
        typedef Dune::MultipleCodimMultipleGeomTypeMapper<GridView,DGLayout> Mapper;
        Mapper mapper(gridview);

        // iterate over all entities of the grid
        for (const auto& e : elements(gridview))
        {
            auto geo = e.geometry();
            auto gt = geo.type();
            const int cell_index = mapper.map(e);

            std::cout << std::endl;
            std::cout << "Current cell is: " << cell_index << ". The geometry type is: " << gt << std::endl;
            // Calculate the center of the cell by summing up all corner vectors
            // and dividing by the number of corners.  Print the results.
            // Also print the center as returned by center() method of the geometry.
            // *** ADD CODE HERE (1/5) ***

            std::cout << std::endl;
            // iterate over intersections of current entity
            for (const auto& is : intersections(gridview, e))
            {
                auto fgeo = e.geometry();
                auto fgt = fgeo.type();
                auto fgeo_self = is.geometryInInside();

                std::cout << " Intersection endpoints: ";
                // Output the *global* corner and center coordinates of the intersection
                // *** ADD CODE HERE (2/5) ***

                if (is.neighbor())       // intersection with a neighboring cell
                {
                    // get the index of neighboring cell as returned by the mapper
                    // *** ADD CODE HERE (3/5) ***
                    std::cout << std::endl;

                    // The coordinate of the center of an intersection with respect to the intersection itself is 0.5.
                    // You can as well extract it from the reference element:
                    Dune::FieldVector<double,1> localcenter =
                      Dune::ReferenceElements<double,1>::general(fgt).position(0,0);

                    // Furthermore, the very same point can be expressed 
                    // 1.) in global coordinates with respect to the domain origin,
                    // 2.) in coordinates with respect to the current element or
                    // 3.) in coordinates with resepct to the neighboring element.
                    // *** ADD CODE HERE (4/5) ***
                }
                else if (is.boundary())  // intersection with the boundary
                {
                    std::cout << " Boundary intersection center = 0.5";
                    // get the global coordinates of the center of the intersection
                    // and the coordinates with resepct to the owning element
                    // *** ADD CODE HERE (5/5) ***
                    std::cout << std::endl;
                }
            }
        }
    }
    // catch exceptions
    catch (Dune::Exception &e){
        std::cerr << "Dune reported error: " << e << std::endl;
    }
    catch (...){
        std::cerr << "Unknown exception thrown!" << std::endl;
    }
}
